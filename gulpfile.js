'use strict';
 
// import { task, src, dest, watch } from "gulp";
// import sass, { logError } from "gulp-sass";
var gulp = require('gulp');
var sass = require('gulp-sass');
gulp.task('sass', function () {
  return gulp.src('./src/assets/scss/app.scss')
    .pipe(sass().on('error', sass.logError))
    .pipe(gulp.dest('./src/assets/css'));
});
 
gulp.task('sass:watch', function () {
    gulp.watch('./src/assets/scss/**/*.scss', ['sass']);
});