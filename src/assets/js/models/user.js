export default class User {
    constructor(name, website, phone, address, rating, reviews, followers) {
      this.name = name;
      this.website = website;
      this.phone = phone;
      this.address = address;
      this.rating = rating;
      this.reviews = reviews;
      this.followers = followers;
    }
  }