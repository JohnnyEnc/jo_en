let input = (type,label, model) => {

    let template = `<div class="group">      
    <input class="material_input" type="${type}" model="${model}" required>
    <span class="bar"></span>
    <label  class="material_label">${label}</label>
  </div>`;
  return template;

}

export default {
    buildInput(type,label, model) {
        console.log(model);
      return input(type, label, model);
    }
  };