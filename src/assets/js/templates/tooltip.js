import input from "./inputs";

let arr = [];

const tooltip = el => {
  let type = el.parentNode.getAttribute("type")
      ? el.parentNode.getAttribute("type")
      : "text",
    label = el.parentNode.getAttribute("label")
      ? el.parentNode.getAttribute("label")
      : "label",
      model = el.parentNode.getAttribute("model");
console.log(model);
  arr.forEach(elm => {
    elm.className = "tooltip hide";
  });
  let createdTooltip = document.getElementById(`tooltip_${el.parentNode.id}`);
  if (!createdTooltip) {
    let tooltipEl = document.createElement("div");
    tooltipEl.id = `tooltip_${el.parentNode.id}`;
    tooltipEl.className = "tooltip show";
    tooltipEl.style.marginLeft = `${el.offsetLeft + 30}px`;
    tooltipEl.innerHTML = buildHtml(type, label, model);
    tooltipEl.innerHTML += buildButtons(el.parentNode.id);
    el.parentNode.appendChild(tooltipEl);
    arr.push(document.getElementById(tooltipEl.id));
  } else {
    let className =
      createdTooltip.className === "tooltip show"
        ? "tooltip hide"
        : "tooltip show";
    createdTooltip.className = className;
  }


document.getElementById(`cancel_${el.parentNode.id}`).addEventListener("click", closeTooltip, false);

};
const buildButtons = (tooltipId) =>
{
    let container = "<div class='tooltip_buttons'>";    
    let saveBtn = '<input type="submit" class="tooltip_button primary_button" value="SAVE">',
    cancelBtn = `<input type="button" class="tooltip_button secondary_button" value="CANCEL" id="cancel_${tooltipId}">`;

    container += saveBtn + cancelBtn;
    container += "</div>"
    return container;
    
}
const closeTooltip = (e) =>
{
    e.target.parentNode.parentNode.className = "tooltip hide";
}

const buildHtml = (type, label, model) => {
  return input.buildInput(type, label, model);
};
export default {
  buildTooltip(el) {
    tooltip(el);
  }
};
