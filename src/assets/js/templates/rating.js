let element = document.getElementsByTagName("rating")[0];

const rating = (qty) =>
{
    let template = `<ul class="container__card__profile__info-container__rating__rating-list">`;
    for (let index = 0; index < 5; index++) {      
       
        template += `<li class="container__card__profile__info-container__rating__rating-list__rating-list-item">
            <i class="${index < qty ? 'ion-android-star' : 'ion-android-star-outline'} rating-star"></i>
       </li>`;
    }
    template += "</ul>";
    element.innerHTML = template;
}
export default{
    buildRating(qty)
    {
        rating(qty);
    }
}
    
    
