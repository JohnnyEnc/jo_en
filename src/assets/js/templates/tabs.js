import tooltip from "./tooltip";
import Utils from "../utils/utils";

let element = document.querySelectorAll("[tabs]")[0];
let tabContainer = document.querySelectorAll("[tabContainer]")[0];

let touchstartX = 0,
  touchstartY = 0,
  touchendX = 0,
  touchendY = 0,
  direction = "";

const tabs = arr => {
  let template = "";

  arr.forEach(element => {
    template += `<li content="${
      element.title
    }" class="container__card__profile__info-container__tabs-container__tabs__tabs-item ${
      element.active ? "tabs-item--active" : ""
    } ">
            ${element.title}
        </li>`;
  });
  buildContent(arr);

  element.innerHTML = template;
};
let buildContent = el => {
  el.forEach(element => {
    let template = `<div tabContent content="${element.title}" class="${
      element.active ? "tab__container--show" : "tab__container--hidden"
    }"><h3 class="container__card-content__title" >${
      element.title
    }  <i class="ion-edit icon-right mobile" edit></i></h3>
    <div class="container__card-content__body">
    ${element.content ? element.content : ""}
    </div></div>
    `;

    tabContainer.innerHTML += template;
  });
};
document.addEventListener("DOMContentLoaded", function() {
  let setActive = e => {
    console.log(e);
    let active = document.getElementsByClassName("tabs-item--active")[0];
    active.className = active.className.replace("tabs-item--active", "");
    e.className += " tabs-item--active";

    let container = e.getAttribute("content");
    let activeContent = document.getElementsByClassName(
      `tab__container--show`
    )[0];
    activeContent.className = activeContent.className.replace(
      "tab__container--show",
      "tab__container--hidden"
    );
    let activeContainer = document.querySelectorAll(
      `div[content="${container}"]`
    )[0];
    activeContainer.className = activeContainer.className.replace(
      "tab__container--hidden",
      "tab__container--show"
    );
  };
  let childrenArr = Array.from(element.children);

  tabContainer.addEventListener(
    "touchstart",
    function(event) {
      touchstartX = event.changedTouches[0].screenX;
    },
    false
  );

  tabContainer.addEventListener(
    "touchend",
    function(event) {
      touchendX = event.changedTouches[0].screenX;
      handleGesture(event);
    },
    false
  );

  function handleGesture(evt) {
    const { width, height } = tabContainer.getBoundingClientRect();

    const ratio_horizontal = (touchendX - touchstartX) / width;
    const ratio_vertical = (touchendY - touchstartY) / height;

    let active = document.getElementsByClassName("tabs-item--active")[0]
      .innerHTML;
    let obj = childrenArr.find(o => o.innerHTML === active);
    let idx = childrenArr.indexOf(obj);
    if (ratio_horizontal > ratio_vertical && ratio_horizontal > 0.25) {
      if (idx + 1 < childrenArr.length) {
        setActive(childrenArr[idx + 1]);
      }
    }

    if (ratio_horizontal < ratio_vertical && ratio_horizontal < -0.25) {
      if (idx - 1 >= 0) {
        setActive(childrenArr[idx - 1]);
      }
   
    }
  }

  for (let index = 0; index < childrenArr.length; index++) {
    childrenArr[index].addEventListener(
      "click",
      function(evt) {
        setActive(evt.target);
      },
      false
    );
  }

  let edits = document.querySelectorAll("[edit]");
  edits.forEach(element => {
    element.addEventListener(
      "click",
      function() {
        tooltip.buildTooltip(element);
      },
      false
    );
  });
});

export default {
  buildTabs(arr) {
    tabs(arr);
  }
};
Utils.twoWayBinding({ name: "name" });
