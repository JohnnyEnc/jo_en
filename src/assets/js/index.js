import Polyfills from "./utils/polyfills";

import User from "./models/user";
import rating from "./templates/rating";
import tabsBuilder from "./templates/tabs";
import Utils from "./utils/utils";


let user = new User(
  "Jessica Parker",
  "www.seller.com",
  "(849) 325-68594",
  "Newport Beachm CA",
  4,
  6,
  15
);

let tabs = [
  {
    title: "ABOUT",
    active: true,
    content: ` <div id="name" class="container__card-content__body__control" type="text" label="Name" model="name">
                <strong data="name"></strong>
                <i class="ion-edit icon-right" edit></i>
            </div>
            <div id="website"  class="container__card-content__body__control" type="text" label="Website" model="website">
            <i class="ion-android-globe"></i>
                <span data="website"></span>
                <i class="ion-edit icon-right" edit></i>
            </div>
            <div id="phone" class="container__card-content__body__control" type="text" label="Phone Number" model="phone">
            <i class="ion-ios-telephone-outline"></i>
            
                <span data="phone"></span>
                <i class="ion-edit icon-right" edit></i>
            </div>
            <div id="address" class="container__card-content__body__control" type="text" label="Address" model="address">
            <i class="ion-ios-location-outline"></i>
            
                <span data="address"></span>
                <i class="ion-edit icon-right" edit></i>
            </div>` 
  },
  {
    title: "SETTINGS",
    content:""
  },
  {
    title: "OPTION1",
    content:""
  },
  {
    title: "OPTION2",
    content:""
    
  },
  {
    title: "OPTION3",
    content:""
    
  }
];

let fillUserInfo = () => {
  rating.buildRating(user.rating);
  tabsBuilder.buildTabs(tabs);

  for (const key in user) {
    if (user.hasOwnProperty(key)) {
      let el = document.querySelectorAll(`[data='${key}']`);
      if (key !== "rating") {
        el.forEach(element => {
          if (element !== undefined) {
            element.innerHTML += user[key];
          }
        });

          
      }
      if (key === "reviews") {
        el[0].innerHTML = user[key] + " Reviews";
      }
    }
  }

};



fillUserInfo();
