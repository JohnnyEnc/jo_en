var $scope = {};

let utils = obj => {
  let keys = Object.keys(obj);
  bindEvent(keys);
};
let bindEvent = keys => {
  keys.forEach(element => {
    let elms = document.querySelectorAll(`input[model=${element}]`);
    for (const key in elms) {
      if (elms.hasOwnProperty(key)) {
        elms[key].onkeyup = () => {
            console.log("test");
          for (const _key in elms) {
            if (elms.hasOwnProperty(_key)) {
              elms[_key].value = this.value;
            }
          }
        };
      }
    }
    Object.defineProperty($scope, element, {
        set: function (newValue) {
            for (var index in elements) {
                elements[index].value = newValue;
            }
        }
    });
  });
};

export default {
  twoWayBinding(obj) {
    return utils(obj);
  }
};

// import User from "./models/user";

// var $scope = {};
// (function () {
//     var bindClasses = ["name", "age"];
//     var attachEvent = function (classNames) {
//         classNames.forEach(function (className) {
//             var elements = document.getElementsByClassName(className);
//             for (var index in elements) {
//                 elements[index].onkeyup = function () {
//                     for (var index in elements) {
//                         elements[index].value = this.value;
//                     }
//                 }
//             }
//             Object.defineProperty($scope, className, {
//                 set: function (newValue) {
//                     for (var index in elements) {
//                         elements[index].value = newValue;
//                     }
//                 }
//             });
//         });
//     };
//     attachEvent(bindClasses);
// })();
